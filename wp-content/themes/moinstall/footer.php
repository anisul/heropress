			<!-- FOOTER : begin -->
			<div id="footer">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-md-push-6">

							<!-- FOOTER MENU : begin -->
							<nav class="footer-menu">
								<ul>
									<li><a href="#">Home</a></li>
									<li><a href="#">Elements</a></li>
									<li><a href="#">Documentation</a></li>
								</ul>
							</nav>
							<!-- FOOTER MENU : end -->

						</div>
						<div class="col-md-6 col-md-pull-6">

							<!-- FOOTER TEXT : begin -->
							<div class="footer-text">
								
							</div>
							<!-- FOOTER TEXT : end -->

						</div>
					</div>
				</div>
			</div>
			<!-- FOOTER : end -->

		</div>
		<!-- BODY : end -->
<?php wp_footer(); ?>
</body>
</html>