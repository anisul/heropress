<?php get_header(); ?>

		<!-- BODY : begin -->
		<div id="body">

			<!-- WRAPPER : begin -->
			<div id="wrapper" class="m-large-header">

				<!-- CORE : begin -->
				<div id="core">

					<!-- PAGE CONTENT : begin -->
					<div id="page-content">
						<div class="various-content">

							<!-- FEATURES : begin -->
							<section class="c-section m-force-margin">
								<div class="section-inner">

									<div class="container">
										<div class="row">
											<div class="col-sm-4">

												<!-- ICON BLOCK : begin -->
												<div class="c-icon-block">
													<i class="fa fa-html5"></i>
													<h3>Colorful HTML5 Template</h3>
													<p>For Creative Folks or Companies</p>
												</div>
												<!-- ICON BLOCK : end -->

											</div>
											<div class="col-sm-4">

												<!-- ICON BLOCK : begin -->
												<div class="c-icon-block">
													<i class="fa fa-code"></i>
													<h3>Clean and Valid Code</h3>
													<p>Very Easy to Read and Customize</p>
												</div>
												<!-- ICON BLOCK : end -->

											</div>
											<div class="col-sm-4">

												<!-- ICON BLOCK : begin -->
												<div class="c-icon-block">
													<i class="fa fa-paper-plane-o"></i>
													<h3>Coded with SASS</h3>
													<p>Create Custom Skins in Minutes</p>
												</div>
												<!-- ICON BLOCK : end -->

											</div>
										</div>
									</div>
										<hr class="c-divider m-small m-transparent">
										<p class="textalign-center"><a href="#" class="c-button m-outline m-medium">Join Now</a></p>
								</div>
							</section>
							<!-- FEATURES : end -->

							<hr class="c-divider m-negative-small m-transparent">

							<!-- GALLERY : begin -->

							<!-- GALLERY : end -->

							<!-- SERVICES SECTION : begin -->
							<section class="c-section">
								<div class="section-inner">

									<header class="section-header textalign-center">
										<div class="container">
											<h2>Enjoy Moinstall</h2>
										</div>
									</header>

									<div class="container">
										<div class="row">
											<div class="col-md-4">

												<!-- SERVICE : begin -->
												<div class="c-service">
													<p class="service-image m-animated"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/service_01.jpg" alt=""></p>
													<div class="service-ico"><i class="fa fa-rocket"></i></div>
													<div class="service-content">
														<h3>Ajax Contact Form</h3>
														<p>It is a paradisematic country, in which roasted parts of sentences.</p>
														<p><a href="#" class="c-button m-outline">Learn More</a></p>
													</div>
												</div>
												<!-- SERVICE : end -->

											</div>
											<div class="col-md-4">

												<!-- SERVICE : begin -->
												<div class="c-service">
													<p class="service-image m-animated"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/service_02.jpg" alt=""></p>
													<div class="service-ico"><i class="fa fa-cloud"></i></div>
													<div class="service-content">
														<h3>MailChimp Integration</h3>
														<p>Far far away, behind the word mountains, far from the countries.</p>
														<p><a href="#" class="c-button m-outline">Learn More</a></p>
													</div>
												</div>
												<!-- SERVICE : end -->

											</div>
											<div class="col-md-4">

												<!-- SERVICE : begin -->
												<div class="c-service">
													<p class="service-image m-animated"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/service_03.jpg" alt=""></p>
													<div class="service-ico"><i class="fa fa-share-alt"></i></div>
													<div class="service-content">
														<h3>Social Feeds</h3>
														<p>A small river named Duden flows by their place and supplies.</p>
														<p><a href="#" class="c-button m-outline">Learn More</a></p>
													</div>
												</div>
												<!-- SERVICE : end -->

											</div>
										</div>
									</div>

								</div>
							</section>
							<!-- SERVICES SECTION : end -->

							<!-- PARALLAX SECTION : begin -->
							<section class="c-parallax-section m-dynamic" style="display: block; background-image: url(<?php echo esc_url( get_template_directory_uri() ); ?>/img/usrs.jpg); ">
								<div class="section-inner">

									<header class="section-header textalign-center">
										<div class="container">
											<h2>New Horizons Await</h2>
										</div>
									</header>

									<div class="container">
										<div class="row">
											<div class="col-sm-3">

												<!-- COUNTER : begin -->
												<div class="c-counter" data-duration="1000">
													<div class="counter-icon"><i class="fa fa-cog"></i></div>
													<div class="counter-data"><span class="counter-value">3500</span>+</div>
													<span class="counter-label">lines of code</span>
												</div>
												<!-- COUNTER : end -->

											</div>
											<div class="col-sm-3">

												<!-- COUNTER : begin -->
												<div class="c-counter" data-duration="1000">
													<div class="counter-icon"><i class="fa fa-clock-o"></i></div>
													<div class="counter-data"><span class="counter-value">300</span>+</div>
													<span class="counter-label">hours of work</span>
												</div>
												<!-- COUNTER : end -->

											</div>
											<div class="col-sm-3">

												<!-- COUNTER : begin -->
												<div class="c-counter" data-duration="1000">
													<div class="counter-icon"><i class="fa fa-html5"></i></div>
													<div class="counter-data"><span class="counter-value">30</span>+</div>
													<span class="counter-label">HTML files</span>
												</div>
												<!-- COUNTER : end -->

											</div>
											<div class="col-sm-3">

												<!-- COUNTER : begin -->
												<div class="c-counter" data-duration="1000">
													<div class="counter-icon"><i class="fa fa-tint"></i></div>
													<div class="counter-data"><span class="counter-value">4</span></div>
													<span class="counter-label">color schemes</span>
												</div>
												<!-- COUNTER : end -->

											</div>
										</div>
										<hr class="c-divider m-small m-transparent">
										<p class="textalign-center"><a href="#" class="c-button m-outline m-medium">Join Today</a></p>
									</div>

								</div>
							</section>
							<!-- PARALLAX SECTION : end -->

							<!-- BRANDS SECTION : begin -->
							<section class="c-section">
								<div class="section-inner">

									<header class="section-header textalign-center">
										<div class="container">
											<h2>Our Clients</h2>
										</div>
									</header>

									<div class="container">
										<div class="row">
											<div class="col-sm-12">

												<!-- BRAND : begin -->
												<p class="textalign-center"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/partners.png" alt=""></p>
												<!-- BRAND : end -->

											</div>
										</div>
									</div>

									<p class="textalign-center"><a href="#" class="c-button m-outline m-medium">Join Them</a></p>

								</div>
							</section>
							<!-- BRANDS SECTION : end -->

						</div>
					</div>
					<!-- PAGE CONTENT : end -->

				</div>
				<!-- CORE : end -->

				<!-- BOTTOM PANEL : begin -->
				<div id="bottom-panel">

					<div class="container">
						<div class="row">
							<div class="col-md-4">

								<!-- TEXT WIDGET : begin -->
								<div class="widget text-widget">
									<h3 class="widget-title">About Moinstall</h3>
									<div class="widget-content">

										<div class="various-content">
											<p><strong>Moinstall</strong> is an ideal template for creative folks or design agencies. <strong>Clean code and top-notch web design techniques</strong> are wrapped with several gorgeous color schemes.</p>

										</div>

									</div>
								</div>
								<!-- TEXT WIDGET : end -->

							</div>
							<div class="col-md-4">

								<!-- BLOG WIDGET : begin -->
								<div class="widget blog-widget">
									<h3 class="widget-title">Latest Posts</h3>
									<div class="widget-content">

										<ul>
											<li>
												<h4><a href="#">The Big Oxmox advised her</a></h4>
												<p class="date">August 24, 2014</p>
											</li>
											<li>
												<h4><a href="#">A small river named Duden</a></h4>
												<p class="date">August 23, 2014</p>
											</li>
											<li>
												<h4><a href="#">One day however a small line</a></h4>
												<p class="date">August 22, 2014</p>
											</li>
										</ul>

									</div>
								</div>
								<!-- BLOG WIDGET : end -->

							</div>
							<div class="col-md-4">

								<!-- SUBSCRIBE WIDGET : begin -->
								<div class="widget subscribe-widget">
									<h3 class="widget-title">Newsletter</h3>
									<div class="widget-content">

										<p><strong>Moinstall</strong> contains fully working <strong>MailChimp</strong> subscribe form.</p>

										<form class="subscribe-widget-form" action="http://volovar.us8.list-manage.com/subscribe/post-json?u=76a50c9454ec8ab78914d1bf2&id=49e892f53d&c=?" method="get">

											<!-- VALIDATION ERROR MESSAGE : begin -->
											<p style="display: none;" class="c-alert-message m-warning m-validation-error"><i class="ico fa fa-exclamation-circle"></i>Email address is required.</p>
											<!-- VALIDATION ERROR MESSAGE : end -->

											<!-- SENDING REQUEST ERROR MESSAGE : begin -->
											<p style="display: none;" class="c-alert-message m-warning m-request-error"><i class="ico fa fa-exclamation-circle"></i>There was a connection problem. Try again later.</p>
											<!-- SENDING REQUEST ERROR MESSAGE : end -->

											<!-- SUCCESS MESSAGE : begin -->
											<p style="display: none;" class="c-alert-message m-success"><i class="ico fa fa-check-circle"></i><strong>Sent successfully.</strong></p>
											<!-- SUCCESS MESSAGE : end -->

											<div class="form-fields">
												<input class="m-required m-email m-has-placeholder" name="EMAIL" type="text" data-placeholder="Your email address" title="Your email address">
												<button class="c-button m-outline submit-btn" type="submit" data-label="Subscribe" data-loading-label="Sending...">Subscribe</button>
											</div>

										</form>

									</div>
								</div>
								<!-- SUBSCRIBE WIDGET : end -->

							</div>
						</div>
					</div>

					<!-- BACK TO TOP : begin -->
					<a href="#header" id="back-to-top" title="Back to top"><i class="fa fa-chevron-up"></i></a>
					<!-- BACK TO TOP : end -->

				</div>
				<!-- BOTTOM PANEL : end -->

			</div>
			<!-- WRAPPER : end -->



<?php get_sidebar(); ?>
<?php get_footer(); ?>